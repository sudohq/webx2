import { Webx2Page } from './app.po';

describe('webx2 App', () => {
  let page: Webx2Page;

  beforeEach(() => {
    page = new Webx2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
