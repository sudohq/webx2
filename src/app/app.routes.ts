import { Route } from '@angular/router';

import { AppComponent } from './app.component';
import { CanActivateMain } from './services/auth.guard.service';


import { MainComponent } from './modules/main/components/main/main.component';
import { AuthComponent } from './modules/auth/components/auth/auth.component';

import { MainRoutes } from './modules/main/main.routes';
import { AuthRoutes } from './modules/auth/auth.routes';

export const AppRoutes:Route[] = [
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: '',
        component: MainComponent,
        canActivate: [CanActivateMain],
        children: MainRoutes
      },
      {
        path: 'auth',
        component: AuthComponent,
        children: AuthRoutes
      }
    ]
  }
];
