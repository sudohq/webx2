import { Route } from '@angular/router';
import { MainComponent } from './components/main/main.component';

import { ConversationComponent } from './components/conversation/conversation/conversation.component';
import { ConversationInitialComponent } from './components/conversation/conversation-initial/conversation-initial.component';
import { DirectoryComponent } from './components/directory/directory/directory.component';
import { NotificationComponent } from './components/notification/notification/notification.component';
import { ArchiveComponent } from './components/archive/archive/archive.component';
import { HistoryComponent } from './components/history/history/history.component';
import { AppComponent } from '../../app.component';

import { CanActivateMain } from './main.guard.service';
import { UserResolveService } from './services';



export const MainRoutes:Route[] = [
{
  path: '',
  component: AppComponent,
  resolve: UserResolveService,
  children: [
    {
      path: '',
      component: MainComponent,
      canActivate: [CanActivateMain],
      children: [
        {
          path: 'conversations',
          component: ConversationInitialComponent
        },
        {
          path: 'conversations/:id',
          component: ConversationComponent
        },
        {
          path: 'directory',
          component: DirectoryComponent
        },
        {
          path: 'notifications',
          component: NotificationComponent
        },
        {
          path: 'archive',
          component: ArchiveComponent
        },
        {
          path: 'history',
          component: HistoryComponent
        }
      ]
    }
  ]
}];
