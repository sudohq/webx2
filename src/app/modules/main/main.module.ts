import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MomentModule } from 'angular2-moment';
import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';


import { MainRoutes } from './main.routes';
import { CanActivateMain } from './main.guard.service';
import { DirectoryService } from './services/directory.service';
import { ConversationsService } from './services/conversations.service';
import { UserService } from './services/user.service';
import { SignalrService } from './services/signalr.service';
import { SettingsService } from './services/settings.service';

import { MainComponent } from './components/main/main.component';
import { ConversationComponent } from './components/conversation/conversation/conversation.component';
import { ConversationMessageListComponent } from './components/conversation/conversation-message-list/conversation-message-list.component';
import { ConversationMessageComponent } from './components/conversation/conversation-message/conversation-message.component';
import { ConversationTopBarComponent } from './components/conversation/conversation-top-bar/conversation-top-bar.component';
import { ConversationMessageBarComponent } from './components/conversation/conversation-message-bar/conversation-message-bar.component';
import { ConversationTabComponent } from './components/conversation/conversation-tab/conversation-tab.component';
import { ConversationTabItemComponent } from './components/conversation/conversation-tab-item/conversation-tab-item.component';
import { ConversationTabItemListComponent } from './components/conversation/conversation-tab-item-list/conversation-tab-item-list.component';
import { ConversationTabSearchComponent } from './components/conversation/conversation-tab-search/conversation-tab-search.component';
import { ConversationTabSortComponent } from './components/conversation/conversation-tab-sort/conversation-tab-sort.component';
import { DirectoryComponent } from './components/directory/directory/directory.component';
import { DirectoryTabComponent } from './components/directory/directory-tab/directory-tab.component';
import { DirectoryTabItemComponent } from './components/directory/directory-tab-item/directory-tab-item.component';
import { DirectoryTabSearchComponent } from './components/directory/directory-tab-search/directory-tab-search.component';
import { DirectoryTabSortComponent } from './components/directory/directory-tab-sort/directory-tab-sort.component';
import { HistoryTabComponent } from './components/history/history-tab/history-tab.component';
import { HistoryComponent } from './components/history/history/history.component';
import { NotificationComponent } from './components/notification/notification/notification.component';
import { NotificationTabComponent } from './components/notification/notification-tab/notification-tab.component';
import { ArchiveComponent } from './components/archive/archive/archive.component';
import { ArchiveTabComponent } from './components/archive/archive-tab/archive-tab.component';
import { TabBarComponent } from './components/tab-bar/tab-bar.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { ConversationInitialComponent } from './components/conversation/conversation-initial/conversation-initial.component';
import { MessageThumbnailComponent } from './components/message-thumbnail/message-thumbnail.component';
import { NumberSortPipe } from './pipes/number-sort.pipe';
import { SignalRModule } from 'ng2-signalr';
import { ScrollerDirective } from './directives/scroller.directive';
import { AutosizeDirective } from './directives/autosize.directive';
import { ConversationMessageBarAttachmentComponent } from './components/conversation/conversation-message-bar-attachment/conversation-message-bar-attachment.component';
import { InviewDirective } from './directives/inview.directive';

@NgModule({
  imports: [
    MomentModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SignalRModule.forRoot(SignalrService.initConfig),
    RouterModule.forRoot(MainRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    MainComponent,
    ConversationComponent,
    ConversationMessageListComponent,
    ConversationMessageComponent,
    ConversationTopBarComponent,
    ConversationMessageBarComponent,
    ConversationTabComponent,
    ConversationTabItemComponent,
    ConversationTabItemListComponent,
    ConversationTabSearchComponent,
    ConversationTabSortComponent,
    DirectoryComponent,
    DirectoryTabComponent,
    DirectoryTabItemComponent,
    DirectoryTabSearchComponent,
    DirectoryTabSortComponent,
    HistoryTabComponent,
    HistoryComponent,
    NotificationComponent,
    NotificationTabComponent,
    ArchiveComponent,
    ArchiveTabComponent,
    TabBarComponent,
    NavBarComponent,
    ConversationInitialComponent,
    MessageThumbnailComponent,
    NumberSortPipe,
    ScrollerDirective,
    AutosizeDirective,
    ConversationMessageBarAttachmentComponent,
    InviewDirective
  ],
  providers: [
    CanActivateMain,
    DirectoryService,
    ConversationsService,
    UserService,
    SignalrService,
    SettingsService
  ]
})
export class MainModule { }
