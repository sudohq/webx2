import { Directive, ElementRef, Input, AfterViewInit, OnDestroy, OnChanges, Output, EventEmitter, SimpleChanges} from '@angular/core';
import { Observable, Subscription } from 'rxjs';

@Directive({
  selector: '[appInview]'
})
export class InviewDirective implements AfterViewInit, OnDestroy {
  @Input()  parentSelector: string;
  @Input()  isEnabled: boolean = false;

  @Output() appInview: EventEmitter<void> = new EventEmitter<void>();

  self: Element;
  parent: Element;
  sub: Subscription

  constructor(el: ElementRef) {
    this.self = el.nativeElement;
  }

  init() {
    if (this.parentSelector) {
      this.parent = document.querySelector(this.parentSelector);

      if (!this.parent) {
        throw new Error('No parent selector find');
      }
    } else {
      this.parent = this.self.parentElement;
    }

    this.checkIsOnView();

    this.sub = Observable.fromEvent(this.parent, 'scroll')
      .subscribe(() => this.checkIsOnView());
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['isEnabled'].currentValue !== this.isEnabled) {
      if (changes['isEnabled'].currentValue) {
        this.init();
      } else {
        if (this.sub) {
          this.sub.unsubscribe();
        }
      }
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.isEnabled) {
          this.init();
      }
    });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  checkIsOnView() {
    let elValue = this.self.getBoundingClientRect();
    let parentValue = this.parent.getBoundingClientRect();

    if (elValue.top >= (parentValue.top - elValue.height) && elValue.top <= (parentValue.top + parentValue.height)) {
      this.appInview.emit();
    }
  }
}
