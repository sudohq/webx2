import { Directive, ElementRef, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appAutosize]'
})
export class AutosizeDirective implements OnInit, OnDestroy {
  observer: MutationObserver;
  elementCopy: any;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.elementCopy = this.element.nativeElement.cloneNode();

    this.observer = new MutationObserver(mutations => {
      mutations.forEach(mutation => {
        this.element.nativeElement.style.height = this.element.nativeElement.scrollHeight + 'px';
      });
    });

    this.observer.observe(this.element.nativeElement, { attributes: true, childList: true, characterData: true });
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }

}
