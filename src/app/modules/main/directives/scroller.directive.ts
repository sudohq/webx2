import { Directive, ElementRef, OnDestroy } from '@angular/core';

@Directive({
  selector: '[scroller]'
})
export class ScrollerDirective implements OnDestroy {
  observer: MutationObserver;

  constructor(private element: ElementRef) {
    this.observer = new MutationObserver(mutations => {
      mutations.forEach(mutation => {
        if (mutation.addedNodes.length) {
          Array.from(mutation.addedNodes).forEach((node: Element) => {
              element.nativeElement.scrollTop += node.clientHeight;
          });
        }
       });
    });

    this.observer.observe(element.nativeElement, {childList: true});
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }
}
