import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberSort',
  pure: false
})
export class NumberSortPipe implements PipeTransform {

  transform(value: any[], reverse?: any): any {
    if (value instanceof Array) {
      value.sort((a:any, b:any) => b - a);

      if (reverse) {
        value.reverse();
      }
    }

    return value;
  }

}
