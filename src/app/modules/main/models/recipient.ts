export class RecipientModel {
  ChatMessageId: number;
  RecipientId: number;
  ArchiveDescription: string;
  StatusModifiedTime: Date;
  DeliveredDate	: Date;
  ViewedDate: Date;
  ArchivedDate: Date;
  Status: number;
  Id: number;
}
