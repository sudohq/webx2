import { ContactModel } from '.';

export class ConversationRecipientModel {
  FirstName: string;
  LastName: string;
  FullName: string;
  Active: boolean;
  Blocked: boolean;
  BlockedMe: boolean;
  City: string;
  Organization: string;
  State: string;
  ProfessionalDegree: string;
  Speciality: string;
  UserType: number;
  RegisteredTime: string;
  AllowedDeviceKind: number;
  Contacts: ContactModel[];
  ImageHash: string;
  ModifiedTime: string;
  InvitedBy: number;
  WorkFax: string;
  WorkPhone: string;
  WorkAddress: string;
  ZipCode: string;
  LicenseNumber: string;
  Npi: string;
  FullNameWithProfDegree: string;
  ProfessionalDegreeFiltered: string;
  Id: number;
  fullName: string

  constructor(data: any) {
    Object.assign(this, data);
    this.fullName = `${this.FirstName} ${this.LastName}`;
  }
}
