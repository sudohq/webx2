import { RecipientModel, MessageAttachmentModel, SettingsModel } from '.';
import * as moment from 'moment'

export class MessageDataModel {
  offsetMs: number = new Date().getTimezoneOffset();
  expireDate: Date;
  ChatId: number;
  SenderId: number;
  SentDate: Date;
  LifeTime: number;
  DeliveryAlert: number;
  Priority: number;
  RecallTime: Date;
  Recalled: boolean;
  Completed: boolean;
  Callback: string;
  Data: string;
  Recipients?: RecipientModel[];
  Attachments?: MessageAttachmentModel[];
  Id?: number;

  constructor(messageDataModel?: MessageDataModel) {
    // Empty if new message
    if (messageDataModel) {
      messageDataModel.SentDate = moment(messageDataModel.SentDate)
        .utcOffset(this.offsetMs)
        .toDate();

      messageDataModel.expireDate = moment(messageDataModel.SentDate)
        .add(messageDataModel.LifeTime, 'hours')
        .toDate();

      if (messageDataModel.RecallTime) {
        messageDataModel.RecallTime = new Date(messageDataModel.RecallTime);
      }

      Object.assign(this, messageDataModel);
    }
  }

  apllyToSettings(settings: SettingsModel, userId: number) {
    this.SenderId = userId;
    this.LifeTime = parseInt(settings.MessageLifetime);
    this.Callback = settings.PhoneCallback;
  }
}
