import { ContactModel } from '.';

export class UserModel {
  Password: string;
  HasPassword: boolean;
  ArchieverExpirationDate: Date;
  IsArchiever: boolean;
  SenderExpirationDate: Date;
  IsSender: boolean;
  Email: string;
  PrimaryEmail: string;
  Phone: string;
  PrimaryPhone: string;
  ActiveText: string;
  FirstName: string;
  LastName: string;
  FullName: string;
  Active: boolean;
  Blocked: boolean;
  BlockedMe: boolean;
  City: string;
  Organization: string;
  State: string;
  ProfessionalDegree: string;
  Speciality: string;
  UserType: number;
  RegisteredTime: Date;
  AllowedDeviceKind: number;
  Contacts: ContactModel[];
  ImageHash: string;
  ModifiedTime: Date;
  InvitedBy: number;
  WorkFax: string;
  WorkPhone: string;
  WorkAddress: string;
  ZipCode: string;
  LicenseNumber: string;
  Npi: string;
  FullNameWithProfDegree: string;
  ProfessionalDegreeFiltered: string;
  Id: number;

  constructor(user: any) {
    user.ArchieverExpirationDate = new Date(user.ArchieverExpirationDate);
    user.SenderExpirationDate = new Date(user.SenderExpirationDate);
    user.RegisteredTime = new Date(user.RegisteredTime);
    user.ModifiedTime = new Date(user.ModifiedTime);

    Object.assign(this, user);
  }
}
