export class MessageAttachmentModel {
  ChatMessageId: number;
  Name: string;
  PreviewGenerated: string;
  FilePath: string;
  FileSize: number;
  Id: number;
}
