import { ConversationDataModel, MessageModel, ConversationRecipientModel } from '.';
import { ReplaySubject, BehaviorSubject, Subject, Observable } from 'rxjs';
import { ConversationsService, UserService } from '../services';
import { Inject, Injectable, ReflectiveInjector } from '@angular/core';

@Injectable()
export class ConversationModel {
  data: ConversationDataModel;

  users: number[] = [];
  users$: Observable<number>;
  usersChange$: Observable<boolean>;
  usersList$: Observable<number[]>;
  userJoin$: Subject<number> = new Subject();
  userLeft$: Subject<number> = new Subject();

  messages: number[] = [];
  messages$: Observable<number>;
  messagesChange$: Observable<boolean>;
  messageUpload$: Subject<number> = new Subject();
  messageReceive$: Subject<number> = new Subject();
  messageLast$: ReplaySubject<number> = new ReplaySubject(1);
  messageRemove$: Subject<number> = new Subject();

  unreadMessages$: BehaviorSubject<number> = new BehaviorSubject(0);

  lastMessage: number;
  lastMessage$: ReplaySubject<number> = new ReplaySubject(1);

  constructor(
      conversationData: ConversationDataModel,
    ) {

    this.data = new ConversationDataModel(conversationData);
    this.users = conversationData.Users.map(user => user.Id);

    this.users$ = Observable
      .from(this.users)
      .merge(this.userJoin$);

    this.messages$ = Observable
      .from(this.messages)
      .merge(this.messageUpload$, this.messageReceive$);

    this.messagesChange$ = Observable
      .merge(this.messageUpload$, this.messageReceive$, this.messageRemove$)
      .mapTo(true);

    this.usersChange$ = Observable
      .merge(this.userJoin$, this.userLeft$)
      .mapTo(true);
  }

  addMessage(message: MessageModel, newMessage: boolean = false) {
    this.messages.push(message.data.Id);

    if (newMessage) {
      this.messageReceive$.next(message.data.Id);
    } else {
      this.messageUpload$.next(message.data.Id);
    }
  }

  recallMessage(id: number) {
    this.messages = this.messages.filter(mId => mId !== id);
    this.messageRemove$.next(id);
  }

  addUser(id: number) {
    this.users.push(id);
    this.userJoin$.next(id);
  }

  removeUser(id: number) {
    this.users = this.users.filter(uId => uId !== id);
    this.userLeft$.next(id);
  }
}
