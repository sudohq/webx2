import { ConversationRecipientModel } from '.';

export class ConversationDataModel {
  Name: string;
  ChatOwner: number;
  MegaBlast: boolean;
  Users: ConversationRecipientModel[];
  Id: number;

  constructor(conversationDataModel: ConversationDataModel) {
    Object.assign(this, conversationDataModel);
  }
}
