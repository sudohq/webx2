export class SettingsModel {
  SMSNotifications: string;
  EmailNotifications: string;
  SecureNotifications: string;
  MessageLifetime: string;
  TimeToView: string;
  AutoResendTime: string;
  AutoLogoutTime: string;
  CallbackNumber: string;
  SetForEachMessages: string;
  LaunchCapturingWithMessanger: string;
  CodePhoneCallback: string;
  PhoneCallback: string;

  constructor(settings: Object) {
    Object.assign(this, settings);
  }
}
