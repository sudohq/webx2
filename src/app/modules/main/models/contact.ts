export class ContactModel {
  Contact: string;
  ContactType: number;
  Confirmed: boolean;
  IsPrimary: boolean;
  Id: number;
}
