import { MessageDataModel, RecipientModel}  from '.';
import { Subject, Observable, Subscription, ReplaySubject } from 'rxjs';

export class MessageModel {
  data: MessageDataModel;
  userId: number = parseInt(window.sessionStorage.getItem('userId'));

  recipients: RecipientModel[] = [];
  recipients$: Observable<RecipientModel>;
  recipientJoin$: Subject<RecipientModel> = new Subject();
  recipientLeft$: Subject<number> = new Subject();
  recipientChange$: Subject<RecipientModel> = new Subject();

  isUnread$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(messageData: MessageDataModel) {
    this.data = new MessageDataModel(messageData);
    this.recipients = this.data.Recipients;
    this.recipients$ = Observable
      .from(this.recipients)
      .merge(this.recipientJoin$);

    this.checkIfUnread();
  }

  addRecipient(recipient: RecipientModel) {
    this.recipients.push(recipient);
    this.recipientJoin$.next(recipient);
  }

  removeRecipient(id: number) {
    this.recipients = this.recipients.filter(r => r.RecipientId !== id);
    this.recipientLeft$.next(id);
  }

  changeRecipient(recipient: RecipientModel) {
    let index = this.recipients.findIndex(r => r.RecipientId === recipient.RecipientId);

    if (index >= 0) {
      this.recipients[index] = recipient;
      this.recipientChange$.next(recipient);
    }
  }

  checkIfUnread() {
    let me = this.recipients.find(r => r.RecipientId === this.userId);

    this.isUnread$.next(true);

    // if (me && me.Status < 2) {
    //   this.isUnread$.next(true);
    // }
  }

  markAsRead(id: number) {
    let me = this.recipients.find(r => r.RecipientId === this.userId);

    if (me) {
      me.Status = 2;
      this.isUnread$.next(false)
    }
  }
}
