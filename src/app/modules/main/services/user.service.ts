import { Injectable } from '@angular/core';
import { ReplaySubject, Subject, Observable, Observer, Subscription } from 'rxjs';
import 'rxjs/add/operator/filter';
import { UserRestService } from '../../../services/rest/user.rest.service';
import { UserModel } from '../models';

@Injectable()
export class UserService {
  user: UserModel;
  user$: ReplaySubject<UserModel> = new ReplaySubject(1);
  userUpdate$: Subject<UserModel> = new Subject();

  constructor(private userRS: UserRestService) {
    this.userRS.getCurrentUser()
      .subscribe((user: Object) => {
        this.user = new UserModel(user);

        this.user$.next(this.user);
        this.user$.complete();

        //Make global for models and services use
        window.sessionStorage.setItem('userId', this.user.Id.toString());
      });
  }

  changeUser(user: UserModel) {
    this.user = user;
    this.userUpdate$.next(user);
  }
}
