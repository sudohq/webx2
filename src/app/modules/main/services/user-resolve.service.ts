import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserModel } from '../models';
import { UserService } from '.';

@Injectable()
export class UserResolveService implements Resolve<UserModel> {
  constructor(private us: UserService) {}

  resolve() {
    return this.us.user$;
  }
}
