import { Injectable } from '@angular/core';
import { UserRestService } from '../../../services/rest/user.rest.service';
import { ReplaySubject, Observable } from 'rxjs';
import { SettingsModel } from '../models';

@Injectable()
export class SettingsService {
  settings: SettingsModel;
  settingsUpdate$: ReplaySubject<SettingsModel> = new ReplaySubject(1);
  settings$: Observable<SettingsModel> = Observable.from(this.settingsUpdate$);

  constructor(private userS: UserRestService) {
    userS.getUserSettings()
      .subscribe((data: Array<Object>) => {
        this.extractSettings(data);

        this.settingsUpdate$.next(this.settings);
      });


    setTimeout(() => {
      this.settingsUpdate$.next(this.settings);
    }, 5000);
  }

  extractSettings(settingsArr: Array<Object>) {
    let settingsoObj = {};

    settingsArr.forEach((item: any) => settingsoObj[item.Key] = item.Value);
    this.settings = new SettingsModel(settingsoObj);
  }

  zipSettings() {
    return Object.keys(this.settings).map((key: string) => {
      return {Key: key, Value: this.settings[key].toString()}
    });
  }

}
