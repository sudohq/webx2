import { Injectable } from '@angular/core';
import { Subject, ReplaySubject, Observable } from 'rxjs';
import { UserRestService } from '../../../services/rest/user.rest.service';
import { ConversationRecipientModel } from '../models';

@Injectable()
export class DirectoryService {
  users: Map<number, ReplaySubject<ConversationRecipientModel>> = new Map();
  userAdd$: Subject<ConversationRecipientModel> = new Subject();
  userUpload$: Subject<ConversationRecipientModel> = new Subject();

  constructor(
   private userRestService: UserRestService
  ) {
    this.__uploadUsers();
  }

  protected __uploadUsers():void {
    let uploader = (recentDate: Date = new Date(0)) => {

      this.userRestService.getUsersFrom(recentDate, 100)
        .subscribe(
          (users: any) => {
            if (users && users.length) {
              //this.__uploadAvatars(users);
              users.forEach((u: any) => this.addUser(u));

              uploader(new Date(users.pop().ModifiedTime));
            } else {
              this.userUpload$.complete();
            }
          }
        );
    };

    uploader();
  }

  protected __uploadAvatars(users: Object[]):void {
    users.forEach((user: any) => {
      user.userImage = this.userRestService.getUserPhotoById(user.Id);
    });
  }

  addUser(user: any, newUser: boolean = false) {
    let userInstance = new ConversationRecipientModel(user);
    let userSubject = this.users.get(user.Id) || new ReplaySubject(1);

    userSubject.next(userInstance);

    this.users.set(user.Id, userSubject);

    if (newUser) {
      this.userAdd$.next(userInstance);
    } else {
      this.userUpload$.next(userInstance);
    }
  }

  getUserById(id: number) {
    return Observable.create(observer => {
      if (this.users.has(id)) {
        observer.next(this.users.get(id));
        observer.complete();
      } else {
        this.users.set(id, new ReplaySubject(1))

        this.userRestService.getUserById(id)
          .subscribe(user => {
            this.addUser(user);

            observer.next(this.users.get(id));
            observer.complete();
          },
          observer.error
        );
      }
    })
    .flatMap(user => user);
  }
}
