import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Observer, Subject, ReplaySubject } from 'rxjs';
import { ConversationsRestService } from '../../../services/rest/conversations.rest.service';
import { UserService } from '.';
import { MessageModel, MessageDataModel, ConversationModel, ConversationDataModel, UserModel } from '../models';

@Injectable()
export class ConversationsService {
  userId: number = parseInt(window.sessionStorage.getItem('userId'));

  messages: Map<number, MessageModel> = new Map();
  messageAdd$: Subject<MessageModel> = new Subject();
  messageRemove$: Subject<number> = new Subject();

  conversations: Map<number, ReplaySubject<ConversationModel>> = new Map();
  conversations$: Observable<ConversationModel>;
  conversationsAdd$: Subject<ConversationModel> = new Subject();

  messagesUpload$: Observable<MessageDataModel[]>;
  messagesUploadCount: number = 50;

  constructor(
    private convRestService: ConversationsRestService,
    private userService: UserService
  ) {
    this.conversations$ = Observable
      .from(Array.from(this.conversations.values()))
      .merge(this.conversationsAdd$);

    this.init();
  }

  // Service init method
  init() {
    this.messagesUpload$ = this.messageUploader()
    this.messagesUpload$.subscribe((messages: MessageDataModel[]) => {
      this.checkUnrecievedMessages(messages)
        .then(() => {
          messages.forEach(m => this.addNewMessage(new MessageModel(m), false));
        })
        .catch(err => console.error('Message check error!'));
    });
  }

  // Check if message in list is not unrecieved status, if true send to s req
  checkUnrecievedMessages(messages: MessageDataModel[]) {
    return new Promise((resolve, reject) => {
      let unr = messages.reduce((list, m) => {
        if (m.Recipients.some(r => r.RecipientId === this.userId && r.Status === 0)) {
          list.push({ MessageId: m.Id, Status: 1 })
        }

        return list;
      }, []);

      if (!unr.length) {
        resolve();
      } else {
        this.convRestService.changeMessageStatus(unr)
          .subscribe(null, reject, resolve);
      }
    });
  }

  // Recursive messages uploader
  messageUploader() {
    let loaderFunc = (observer, date: Date = new Date()) => {
      this.convRestService.getMessagesFrom(date, this.messagesUploadCount)
        .subscribe((messages: MessageDataModel[]) => {
          if (messages.length) {
            observer.next(messages);

            loaderFunc(observer, new Date(messages[messages.length - 1].SentDate));
          } else {
            observer.complete();
          }
        }, (err => console.error('Message upload error')));
      };

    return Observable.create(loaderFunc);
  }

  addNewMessage(message: MessageModel, isNew: boolean = true) {
    this.messages.set(message.data.Id, message);
    this.messageAdd$.next(message);

    let conversation = this.conversations.get(message.data.ChatId);

    if (conversation) {
      conversation.subscribe(conversation => {
        conversation.addMessage(message, isNew);
      });
    } else {
      this.uploadChat(message.data.ChatId, message, isNew)
    }
  }

  uploadChat(id: number, message?: MessageModel, isNew: boolean = false) {
    let subject = new Subject();

    this.conversations.set(id, new ReplaySubject(1));

    this.convRestService.getConversationById(id)
      .subscribe((conversation: ConversationDataModel) => {
        let conversationModel = new ConversationModel(conversation);

        if (message) {
          conversationModel.addMessage(message, isNew);
        }

        this.conversations.get(id).next(conversationModel);
        this.conversationsAdd$.next(conversationModel);
        subject.next(this.conversations.get(id));
      },
      (err) => subject.error(err),
      () => subject.complete()
    );

    return subject;
  }

  getConversationById(id: number): Observable<ConversationModel> {
    return new Observable(observer => {
      let conversation = this.conversations.get(id);

      if (conversation) {
        conversation.subscribe(observer);
      } else {
        this.uploadChat(id).subscribe(observer);
      }
    });
  }

  getMessageById(id: number): MessageModel {
    return this.messages.get(id);
  }

  getUnreadCount(ids: number[]) {
    return ids
      .sort()
      .reverse()
      .map(id => this.messages.get(id))
      .reduce((count, message) => {
        let unread = !!message.data.Recipients.find(r => r.Status < 2);

        count += unread ? 1 : 0;

        return count;
      }, 0);
  }

  getLastMessage(ids: number[]) {
    return ids
      .sort()
      .reverse()
      .map(id => this.messages.get(id))
      .find(m => !!m.data.SenderId);
  }
}
