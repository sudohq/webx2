import { Injectable, Inject, forwardRef } from '@angular/core';
import { SignalR, BroadcastEventListener, SignalRConfiguration } from 'ng2-signalr';
import { environment } from '../../../../environments/environment';
import { AuthService } from '../../../services/auth.service';
import { ConversationsService } from './conversations.service';
import { MessageModel, MessageDataModel } from '../models';

declare var $: any;

@Injectable()
export class SignalrService {
  constructor(
    private _signalR: SignalR,
    private authS: AuthService,
    private convS: ConversationsService
  )  {
    $.signalR.ajaxDefaults.headers = {Authorization: `${authS.getToken()}`};

    let connection = this._signalR.connect();

    let newMessage$ = new BroadcastEventListener<MessageDataModel>('newChatMessage');
    let newMessageStatus$ = new BroadcastEventListener<any>('newMessageStatus');
    let newSettings$ = new BroadcastEventListener<any>('newSettings');
    let newUserEvent$ = new BroadcastEventListener<any>('newSettings');

    let onTyping$ = new BroadcastEventListener<any>('onTyping');
    let onLogout$ = new BroadcastEventListener<any>('logout');
    let onRecall$ = new BroadcastEventListener<any>('messageRecalled');

    connection.then(connect => {
      connect.listen(newMessage$);
      connect.listen(newMessageStatus$);
      connect.listen(newSettings$);
      connect.listen(newUserEvent$);
      connect.listen(onTyping$);
      connect.listen(onLogout$);
      connect.listen(onRecall$);
    })
    .catch(err => {
      console.log(err);
    });

    newMessage$.subscribe((message: MessageDataModel) => {
      let messageObj = new MessageModel(message);

      this.convS.checkUnrecievedMessages([message])
        .then(() => {
          this.convS.addNewMessage(messageObj);
        });
    });

  }

  static initConfig() {
    const c = new SignalRConfiguration();

    c.hubName = 'ChatHub';
    c.url = environment.api;
    c.logging = false;

    return c;
  }
}
