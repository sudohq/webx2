import { Component } from '@angular/core';
import { SignalrService } from '../../services/signalr.service';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
  constructor(
    private signalRS: SignalrService,
    private settings: SettingsService
  ) { }

}
