import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectoryTabSortComponent } from './directory-tab-sort.component';

describe('DirectoryTabSortComponent', () => {
  let component: DirectoryTabSortComponent;
  let fixture: ComponentFixture<DirectoryTabSortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryTabSortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryTabSortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
