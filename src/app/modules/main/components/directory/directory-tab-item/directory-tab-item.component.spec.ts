import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectoryTabItemComponent } from './directory-tab-item.component';

describe('DirectoryTabItemComponent', () => {
  let component: DirectoryTabItemComponent;
  let fixture: ComponentFixture<DirectoryTabItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryTabItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryTabItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
