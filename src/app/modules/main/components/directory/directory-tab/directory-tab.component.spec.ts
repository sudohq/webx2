import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectoryTabComponent } from './directory-tab.component';

describe('DirectoryTabComponent', () => {
  let component: DirectoryTabComponent;
  let fixture: ComponentFixture<DirectoryTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
