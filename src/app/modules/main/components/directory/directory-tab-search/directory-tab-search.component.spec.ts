import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectoryTabSearchComponent } from './directory-tab-search.component';

describe('DirectoryTabSearchComponent', () => {
  let component: DirectoryTabSearchComponent;
  let fixture: ComponentFixture<DirectoryTabSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryTabSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryTabSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
