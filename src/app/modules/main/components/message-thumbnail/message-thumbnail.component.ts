import { Component, OnInit, Input } from '@angular/core';
import { ConversationsRestService } from '../../../../services/rest/conversations.rest.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-message-thumbnail',
  templateUrl: './message-thumbnail.component.html',
  styleUrls: ['./message-thumbnail.component.scss']
})
export class MessageThumbnailComponent implements OnInit {
  @Input() message;

  image: any;
  fileExtension: string;
  allowImageExt: string[] = ['png', 'jpeg', 'jpg', 'capture'];
  allowNotImageExt: string[] = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];

  constructor(
    private sanitizer: DomSanitizer,
    private convRS: ConversationsRestService
  ) { }

  ngOnInit() {
    let imageAttachment = this.message.Attachments
      .find(attachment => {
        let ext = attachment.Name.split('.').pop().toLowerCase();

        return this.allowImageExt.indexOf(ext) > -1;
      })



    if (imageAttachment && imageAttachment.Id) {
      this.fileExtension = 'image';

      this.convRS.getMessageThumbnail(imageAttachment.Id)
        .subscribe(
          blob => this.image = this.sanitizer.bypassSecurityTrustStyle(`url(${URL.createObjectURL(blob)})`),
          console.error
        );
    } else {
      this.fileExtension = 'file';
    }
  }

}
