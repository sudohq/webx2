import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveTabComponent } from './archive-tab.component';

describe('ArchiveTabComponent', () => {
  let component: ArchiveTabComponent;
  let fixture: ComponentFixture<ArchiveTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchiveTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
