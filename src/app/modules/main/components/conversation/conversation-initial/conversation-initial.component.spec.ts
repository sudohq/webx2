import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationInitialComponent } from './conversation-initial.component';

describe('ConversationInitialComponent', () => {
  let component: ConversationInitialComponent;
  let fixture: ComponentFixture<ConversationInitialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationInitialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationInitialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
