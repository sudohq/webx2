import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationTabComponent } from './conversation-tab.component';

describe('ConversationTabComponent', () => {
  let component: ConversationTabComponent;
  let fixture: ComponentFixture<ConversationTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
