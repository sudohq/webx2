import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationTabSearchComponent } from './conversation-tab-search.component';

describe('ConversationTabSearchComponent', () => {
  let component: ConversationTabSearchComponent;
  let fixture: ComponentFixture<ConversationTabSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationTabSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationTabSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
