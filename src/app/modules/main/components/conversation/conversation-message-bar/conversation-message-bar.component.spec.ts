import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationMessageBarComponent } from './conversation-message-bar.component';

describe('ConversationMessageBarComponent', () => {
  let component: ConversationMessageBarComponent;
  let fixture: ComponentFixture<ConversationMessageBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationMessageBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationMessageBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
