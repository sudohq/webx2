import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { ConversationsService } from '../../../services/conversations.service';
import { SettingsService } from '../../../services/settings.service';
import { Observable, Subscription } from 'rxjs';
import { UserModel, SettingsModel, MessageDataModel } from '../../../models';


@Component({
  selector: 'app-conversation-message-bar',
  templateUrl: './conversation-message-bar.component.html',
  styleUrls: ['./conversation-message-bar.component.scss']
})
export class ConversationMessageBarComponent implements OnInit, OnDestroy {
  newMessageForm: FormGroup;
  messageAttachments: File[] = [];
  messageModel: MessageDataModel;
  sub: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userS: UserService,
    private convS: ConversationsService,
    private formBuilder: FormBuilder,
    private settings: SettingsService
  ) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params
      .flatMap(() => {
        this.messageAttachments = [];
        this.messageModel = new MessageDataModel();

        return Observable.combineLatest(this.settings.settings$, this.userS.user$);
      })
      .subscribe((data: [SettingsModel, UserModel]) => {
        this.messageModel.apllyToSettings(data[0], data[1].Id);
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onSubmit(e?: KeyboardEvent) {
    if (e && e.keyCode !== 13 && e.ctrlKey) {
      return false
    }
    console.log(this.messageModel);
  }

  onFileChange(e) {
    let files = Array
      .from(e.target.files)
      .filter((file: File) => !this.messageAttachments.includes(file))
      .forEach((file: File) => this.messageAttachments.push(file));

    this.messageModel.Completed = !!this.messageAttachments.length;

    e.target.value = '';
  }

  removeAttachment(file: File) {
    this.messageAttachments = this.messageAttachments.filter(f => f !== file);
  }
}
