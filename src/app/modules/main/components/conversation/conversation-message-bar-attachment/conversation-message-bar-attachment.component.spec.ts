import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationMessageBarAttachmentComponent } from './conversation-message-bar-attachment.component';

describe('ConversationMessageBarAttachmentComponent', () => {
  let component: ConversationMessageBarAttachmentComponent;
  let fixture: ComponentFixture<ConversationMessageBarAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationMessageBarAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationMessageBarAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
