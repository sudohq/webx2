import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-conversation-message-bar-attachment',
  templateUrl: './conversation-message-bar-attachment.component.html',
  styleUrls: ['./conversation-message-bar-attachment.component.scss']
})
export class ConversationMessageBarAttachmentComponent {
  @Input()  attachment: File;
  @Output() remove:EventEmitter<File> = new EventEmitter<File>();

  constructor() { }

  removeAttachment() {
    this.remove.emit(this.attachment);
  }
}
