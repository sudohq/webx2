import { Component, OnInit, OnDestroy, Input, ViewChildren, ContentChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConversationsService } from '../../../services/conversations.service';
import { Observable } from 'rxjs';
import { ConversationModel } from '../../../models';

@Component({
  selector: 'app-conversation-message-list',
  templateUrl: './conversation-message-list.component.html',
  styleUrls: ['./conversation-message-list.component.scss']
})
export class ConversationMessageListComponent implements OnInit {
  messagesIds: Observable<number[]>;

  constructor(
    private activatedRoute:ActivatedRoute,
    private convS: ConversationsService
  ) {}

  ngOnInit() {
    this.messagesIds = this.activatedRoute.params
      .flatMap(params => this.convS.getConversationById(parseInt(params['id'])))
      .switchMap(conversation => {
        return conversation.messages$.scan((p, c) => p.concat(c), [])
      });
  }
}
