import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationTabItemListComponent } from './conversation-tab-item-list.component';

describe('ConversationTabItemListComponent', () => {
  let component: ConversationTabItemListComponent;
  let fixture: ComponentFixture<ConversationTabItemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationTabItemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationTabItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
