import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConversationsService } from '../../../services/conversations.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-conversation-tab-item-list',
  templateUrl: './conversation-tab-item-list.component.html',
  styleUrls: ['./conversation-tab-item-list.component.scss']
})
export class ConversationTabItemListComponent implements OnInit, OnDestroy{
  private conversationsIds: number[] = [];
  private sub: Subscription;
  constructor(private convS: ConversationsService) { }

  ngOnInit() {
    this.sub = this.convS.conversations$.subscribe(conversation => {
        this.conversationsIds.push(conversation.data.Id);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
