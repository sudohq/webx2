import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { DirectoryService } from '../../../services/directory.service';
import { ConversationsService } from '../../../services/conversations.service';
import { Subscription, Observable } from 'rxjs';
import { MessageAttachmentModel, ConversationModel, MessageModel, ConversationRecipientModel } from '../../../models'

@Component({
  selector: 'app-conversation-tab-item',
  templateUrl: './conversation-tab-item.component.html',
  styleUrls: ['./conversation-tab-item.component.scss']
})
export class ConversationTabItemComponent implements OnInit, OnDestroy {
  @Input() conversationId;

  private conversation: ConversationModel;
  private users$: Observable<number[]>;

  lastMessage: MessageModel;
  unreadCountMessage: number = 0;

  sub: Subscription;

  constructor(
    private ds: DirectoryService,
    private cs: ConversationsService
  ) { }

  ngOnInit() {
    let lastMessageId = 0;

    this.sub = this.cs.getConversationById(this.conversationId)
      .flatMap(conversation => {
        this.conversation = conversation;
        this.users$ = conversation.users$.scan((p, c) => p.concat(c), []);

        this.updateStats();

        return conversation.messagesChange$
      })
      .subscribe(() => {
        this.updateStats();
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  updateStats() {
    this.lastMessage = this.cs.getLastMessage(this.conversation.messages);
    this.unreadCountMessage = this.cs.getUnreadCount(this.conversation.messages);
  }

  isCapture(attachments: MessageAttachmentModel[]): boolean {
    return !!attachments.find(attachment => {
        return attachment.Name.split('.').pop() === 'capture';
    });
  }
}
