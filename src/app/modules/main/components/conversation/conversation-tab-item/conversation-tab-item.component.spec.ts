import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationTabItemComponent } from './conversation-tab-item.component';

describe('ConversationTabItemComponent', () => {
  let component: ConversationTabItemComponent;
  let fixture: ComponentFixture<ConversationTabItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationTabItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationTabItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
