import { Component } from '@angular/core';

@Component({
  selector: 'app-conversation',
  template: `
    <app-conversation-top-bar></app-conversation-top-bar>
    <app-conversation-message-list scroller id="messages-list"></app-conversation-message-list>
    <app-conversation-message-bar></app-conversation-message-bar>
  `,
  styles: [`
    :host {
      display: flex;
      flex-direction: column;
      height: 100%;
      overflow: hidden;
    }
  `]
})
export class ConversationComponent {}
