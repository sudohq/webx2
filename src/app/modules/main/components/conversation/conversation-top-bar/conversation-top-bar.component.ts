import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConversationsService } from '../../../services/conversations.service';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-conversation-top-bar',
  templateUrl: './conversation-top-bar.component.html',
  styleUrls: ['./conversation-top-bar.component.scss']
})
export class ConversationTopBarComponent implements OnInit {
  private conversation: any;

  constructor(
    private activatedRoute:ActivatedRoute,
    private convS: ConversationsService
  ) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe((data: any) => {
        this.convS.getConversationById(+data['id'])
          .subscribe(conversation => {
            this.conversation = conversation;
          });
      });
  }

}
