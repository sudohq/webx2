import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationTopBarComponent } from './conversation-top-bar.component';

describe('ConversationTopBarComponent', () => {
  let component: ConversationTopBarComponent;
  let fixture: ComponentFixture<ConversationTopBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationTopBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationTopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
