import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationTabSortComponent } from './conversation-tab-sort.component';

describe('ConversationTabSortComponent', () => {
  let component: ConversationTabSortComponent;
  let fixture: ComponentFixture<ConversationTabSortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationTabSortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationTabSortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
