import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService, DirectoryService, ConversationsService } from '../../../services';
import { MessageModel, ConversationRecipientModel } from '../../../models';


@Component({
  selector: 'app-conversation-message',
  templateUrl: './conversation-message.component.html',
  styleUrls: ['./conversation-message.component.scss']
})
export class ConversationMessageComponent implements OnInit {
  @Input() messageId;
  userId: number = parseInt(window.sessionStorage.getItem('userId'));
  message: MessageModel;
  messageUser: Observable<ConversationRecipientModel>;
  today: Date = new Date();
  barToggleValue: boolean = false;
  messageUsersStats = [0,0,0,0];

  constructor(
    private us: UserService,
    private ds: DirectoryService,
    private cs: ConversationsService
  ) {}

  ngOnInit() {
    this.message = this.cs.messages.get(this.messageId);
    this.messageUser = this.ds.getUserById(this.message.data.SenderId);

    this.getMessageUsersStats(this.message.recipients);
  }

  barToggle() {
    this.barToggleValue = !this.barToggleValue;
  }

  getMessageUsersStats(recipients: any[]) {
    this.messageUsersStats = [0,0,0,0];

    recipients.forEach(recipient => {
        this.messageUsersStats[recipient.Status] += 1;
    });
  }

  viewed(id: number) {
    this.cs.checkUnrecievedMessages([this.message.data])
      .then(() => {
        this.message.isUnread$.next(false);
      })
      .catch((err) => console.error(err));
  }
}
