import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AuthService } from '../../services/auth.service';


@Injectable()
export class CanActivateMain implements CanActivate {
  constructor(private as: AuthService, private router: Router) {}

  canActivate(): boolean {
    if (this.as.getToken()) {
      return true;
    }

    this.router.navigate(['auth', 'login']);
    return false;
  }
}
