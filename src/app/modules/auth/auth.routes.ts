import { Route } from '@angular/router';
import { RouterModule } from '@angular/router';

import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { RemindComponent } from './components/remind/remind.component';
import { AuthComponent } from './components/auth/auth.component';

export const AuthRoutes:Route[] = [
    {
      path: 'auth',
      component: AuthComponent,
      children: [
        { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
        { path: 'login', component: LoginComponent },
        { path: 'registration', component: RegistrationComponent },
        { path: 'remind-password', component: RemindComponent }
      ]
    }
];
