import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { ReactiveFormsModule } from "@angular/forms";

import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { RemindComponent } from './components/remind/remind.component';
import { AuthComponent } from './components/auth/auth.component';

import { AuthRoutes } from './auth.routes';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(AuthRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [RegistrationComponent, LoginComponent, RemindComponent, AuthComponent]
})
export class AuthModule { }
