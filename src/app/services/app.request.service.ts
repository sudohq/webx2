import { Injectable } from '@angular/core';
import { Http, RequestOptions, XHRBackend } from '@angular/http';

import { AuthService } from './auth.service';

@Injectable()
export class AppRequestService extends Http {
  constructor(
    public _backend: XHRBackend,
    public _options: RequestOptions
  ) {
    super(_backend, _options);
  }

  setAuthHeader(token: string): void {
    this._options.headers.append('Authorization', token);
  }

  removeAuthHeader(): void {
    this._options.headers.delete('Authorization');
  }

  getAuthHeader(): string {
    return this._options.headers.get('Authorization');
  }
}
