import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { UserRestService } from './rest/user.rest.service';
import { AppRequestService } from './app.request.service';

@Injectable()
export class AuthService {
  private token:string = localStorage.getItem('token');
  private authHeaderSecret: string = 'Web';
  private deviceHash: string = this.getDeviceHash();

  constructor(
    private appRequestService:AppRequestService,
    private userRestService: UserRestService,
    private http:Http
  ) {
    this.initSession();
  }

  getDeviceHash():string {
    let deviceHash = localStorage.getItem('deviceHash');

    if (!deviceHash) {
      let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      let hashLength = 64;

      while (hashLength) {
        deviceHash += chars[Math.floor(Math.random() * hashLength)];
        hashLength -= 1;
      }

      localStorage.setItem('deviceHash', deviceHash);
    }

    return deviceHash;
  }

  setToken(username: string, password: string) :Promise<boolean> {
    return new Promise((resolve, reject) => {
      let advanceToken:string = window.btoa(`${username}:${password}:${this.deviceHash}:${this.authHeaderSecret}`);

      this.appRequestService.removeAuthHeader();
      this.appRequestService.setAuthHeader(`Basic ${advanceToken}`);

      this.userRestService.getCurrentUser()
        .subscribe(
          () => {
            this.token = `Basic ${advanceToken}`;

            localStorage.setItem('token', this.token);
            resolve();
          },
          reject
      );
    });
  }

  getToken():string {
    return this.token;
  }

  initSession():void {
    if (this.token) {
      this.appRequestService.setAuthHeader(`${this.token}`);
    }
  }
}
