import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class UserRestService {
  private apiPath = '/api/user/';

  constructor(private http:Http) { }

  getCurrentUser(): Observable<Object> {
    return this.http
      .get(this.apiPath + 'info')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getUserById(userId: number): Observable<Object> {
    return this.http
      .get(this.apiPath + `profile/${userId}`)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getUserPhotoById(userId: number): Observable<Object> {
    return this.http
      .get(this.apiPath + `photo/${userId}`, {
        responseType: ResponseContentType.Blob
      })
      .map(this.extractArrayBuffeData)
      .catch(this.handleError);
  }

  getUsersFrom(time: Date, count?: number): Observable<Object> {
    let params = new URLSearchParams();

    params.set('time', time.toISOString());
    params.set('count', count.toString());

    return this.http
      .get(this.apiPath + 'updates', {search: params})
      .map(this.extractData)
      .catch(this.handleError);
  }

  getUserSettings() {
    return this.http
      .get(this.apiPath + 'settings')
      .map(this.extractData)
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private extractData(res: Response) {
    let body = res.json();

    return body || { };
  }

  private extractArrayBuffeData(res: Response) {
    let body = res.blob();

    return body || { };
  }
}
