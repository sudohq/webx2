import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ConversationsRestService {
  private apiPath = '/api/chat/';

  constructor(private http:Http) { }


  getMessagesFrom(time: Date, count?: number, chatId?: number): Observable<Object> {
    let params = new URLSearchParams();

    params.set('time', time.toISOString());
    params.set('count', count ? count.toString() : null);
    params.set('chatId', chatId ? chatId.toString() : null);

    return this.http
      .get(this.apiPath + 'messages', {search: params})
      .map(this.extractData)
      .catch(this.handleError);
  }

  getConversationById(id: number): Observable<Object> {
    return this.http
      .get(this.apiPath + `${id}`)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getMessageThumbnail(id: number) {
    return this.http
      .get(this.apiPath + 'attachment/thumb/' + `${id}`, {
        responseType: ResponseContentType.Blob
      })
      .map(this.extractArrayBuffeData)
      .catch(this.handleError);
  }

  changeMessageStatus(messages: any[]) {
    return this.http
      .post(this.apiPath + 'notify', messages)
      .catch(this.handleError);
  }

  sendMessage(data: any, completed: boolean) {
    let params = new URLSearchParams();

    params.set('completed', completed.toString());

    return this.http
      .put(this.apiPath + 'message', data)
      .map(this.extractData)
      .catch(this.handleError);
  }

  sendMessageAttachments(id: number, attachments: File[]) {
    let formData = new FormData();

    attachments.forEach((value, i) => formData.append(i.toString(), value));

    return this.http
      .post(this.apiPath + `upload/${id}`, formData)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    return Observable.throw(errMsg);
  }

  private extractData(res: Response) {
    let body = res.json();

    return body || { };
  }

  private extractArrayBuffeData(res: Response) {
    let body = res.blob();

    return body || { };
  }
}
