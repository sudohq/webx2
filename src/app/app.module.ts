import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { RouterModule, Route } from '@angular/router';
import { ReactiveFormsModule } from "@angular/forms";


import { AppComponent } from './app.component';

//appModules
import { AuthModule } from './modules/auth/auth.module';
import { MainModule } from './modules/main/main.module';

//appServices
import { AuthService } from './services/auth.service';
import { AppRequestService } from './services/app.request.service';
import { UserRestService } from './services/rest/user.rest.service';
import { ConversationsRestService } from './services/rest/conversations.rest.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    //appModules
    AuthModule,
    MainModule
  ],
  providers: [
    AuthService,
    AppRequestService,
    UserRestService,
    ConversationsRestService,
    { provide: Http, useClass: AppRequestService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
